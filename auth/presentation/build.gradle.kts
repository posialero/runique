plugins {
    alias(libs.plugins.runique.android.feature.ui)
}

android {
    namespace = "pl.najdrowski.auth.presentation"
}

dependencies {
    implementation(libs.timber)

    implementation(projects.core.domain)
    implementation(projects.auth.domain)
}