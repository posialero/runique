package pl.najdrowski.convention

enum class ExtensionType {
    APPLICATION,
    LIBRARY
}